package com.mvpotter.crawler

import edu.uci.ics.crawler4j.crawler.Page
import edu.uci.ics.crawler4j.crawler.WebCrawler
import edu.uci.ics.crawler4j.parser.HtmlParseData
import edu.uci.ics.crawler4j.url.WebURL
import java.util
import java.util.regex.Pattern

/**
 * @author Mikhail Potter
 * 02.09.2015
 */
class MyCrawler : WebCrawler() {

    companion object: WebCrawler() {
        val FILTERS: Pattern = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp3|zip|gz))$");
    }

    override fun shouldVisit(page: Page?, url: WebURL?): Boolean {
        val href = url?.getURL()?.toLowerCase()
        return !FILTERS.matcher(href).matches()
                && (href != null) && href.startsWith("http://www.ics.uci.edu/")
    }

    override fun visit(page: Page?) {
        val url = page?.getWebURL()?.getURL();
        println("URL: $url")

        if (page != null && page.getParseData() is HtmlParseData) {
            val htmlParseData = page.getParseData() as HtmlParseData
            val text = htmlParseData.getText()
            val html = htmlParseData.getHtml()
            val links = htmlParseData.getOutgoingUrls()

            println("Text length: ${text.length()}")
            println("Html length: ${html.length()}")
            println("Number of outgoing links: ${links.size()}")
        }
    }
}