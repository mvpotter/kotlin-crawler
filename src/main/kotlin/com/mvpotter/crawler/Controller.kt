package com.mvpotter.crawler

import edu.uci.ics.crawler4j.crawler.CrawlConfig
import edu.uci.ics.crawler4j.crawler.CrawlController
import edu.uci.ics.crawler4j.fetcher.PageFetcher
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer

/**
 * @author Mikhail Potter
 * 02.09.2015
 */
fun main(args: Array<String>) {
    val crawlStorageFolder = "data/crawl/root"
    val numberOfCrawlers = 7

    val config = CrawlConfig()
    config.setCrawlStorageFolder(crawlStorageFolder)

    val pageFetcher = PageFetcher(config)
    val robotsTxtConfig = RobotstxtConfig()
    val robotsTxtServer = RobotstxtServer(robotsTxtConfig, pageFetcher)
    val controller = CrawlController(config, pageFetcher, robotsTxtServer)

    controller.addSeed("http://www.ics.uci.edu/~lopes/")
    controller.addSeed("http://www.ics.uci.edu/~welling/")
    controller.addSeed("http://www.ics.uci.edu/")

    controller.start(javaClass<MyCrawler>(), numberOfCrawlers)
}